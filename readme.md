# MUAZZAM RASPBERRY PI - SCRIPT FOR EXTRACTION

## Execute following command.
> ```ssh pi@192.168.10.120 'bash -s' < muazzamRPi-Script.sh  > pi4-1.json```

### Explanation

> ```ssh pi@192.168.10.120```

    This is to connect to RPi using SSH

> ```'bash -s' < muazzamRPi-Script.sh```

    `bash -s` is to open new bash session. with `<` symbol use, it is to input the `muazzamRPi-Script.sh` script tp the bash.


> ```> pi4-1.json```

    This is to get the output, then store it into a file named `pi4-1.json`.
