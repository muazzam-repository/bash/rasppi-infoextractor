# made with <3 by Muazzam
# April 6, 2022
# source for write json using bash: https://serverfault.com/a/873061
# source for getting serial number and model: https://raspberrypi.stackexchange.com/a/2087
# source for getting mac addr eth0 and wlan0: https://www.raspberrypi-spy.co.uk/2012/06/finding-the-mac-address-of-a-raspberry-pi/
# source for getting ram size: https://stackoverflow.com/a/67126302 || https://stackoverflow.com/a/34938001
# obtain serial number
SERIALNUMBER=$(cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2)
# obtain model
## cat /sys/firmware/devicetree/base/model
MODEL=$(cat /proc/cpuinfo | grep Model | cut -d ':' -f 2 | cut -c2-)
# obtain eth0 mac addr
if cat /sys/class/net/eth0/address > /dev/null; then
    ETHMAC=$(cat /sys/class/net/eth0/address)
else
    ETHMAC="null"
fi
# obtain wlan0 mac addr
if cat /sys/class/net/wlan0/address > /dev/null; then
    WLAN0MAC=$(cat /sys/class/net/wlan0/address)
else
    WLAN0MAC="null"
fi
# obtain wlan0 mac addr
if cat /sys/class/net/wlan1/address > /dev/null; then
    WLAN1MAC=$(cat /sys/class/net/wlan1/address)
else
    WLAN1MAC="null"
fi
# obtain memory size (RAM)
# RAM_KB=$(grep MemTotal /proc/meminfo | awk '{print $2}')
# RAM_MB=$(expr $RAM_KB / 1024)
# RAM_GB=$(expr $RAM_MB / 1024)
RAM_GB=$(awk '/MemFree/ { printf "%.0f \n", $2/1024/1024 }' /proc/meminfo | tr -d ' ')
cat > ./config-muazzam.json <<EOF
{
    "RaspberryPi": {
        "serialNumber": "$SERIALNUMBER",
        "model": "$MODEL",
        "eth0mac": "$ETHMAC",
        "wlan0mac": "$WLAN0MAC",
        "wlan1mac": "$WLAN1MAC",
        "ramSize": "$RAM_GB GB"
    }
}
EOF

cat ./config-muazzam.json
